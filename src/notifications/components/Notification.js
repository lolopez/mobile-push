import React from "react";
import { Text, StyleSheet, View, Dimensions } from "react-native";
import PropTypes from "prop-types";

const widtha = Dimensions.get("window").width; // full width

const styles = StyleSheet.create({
  from: {
    fontSize: 20,
    color: "black"
  },
  subject: {
    fontSize: 15,
    color: "black"
  },
  unreadText: {
    fontWeight: "bold"
  },
  notification: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 8,
    marginBottom: 8,
    width: widtha * 0.95
  },
  circle: {
    width: 10,
    height: 10,
    marginTop: 10,
    marginLeft: 5,
    marginRight: 8,
    backgroundColor: "#2196F3",
    borderRadius: 45,
    alignItems: "center",
    justifyContent: "center"
  },
  circleread: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#2196F3"
  },
  text: {
    flex: 1,
    color: "black"
  },
  inlining: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "flex-start",
    justifyContent: "center"
  }
});

const showDate = notification => {
  const notificationDate = new Date(notification.sentAt);
  const today = new Date();
  const hours = notificationDate.getHours();
  const minutes = notificationDate.getMinutes();

  if (notificationDate.toDateString() === today.toDateString()) {
    return `${hours}:${minutes < 10 ? "0" : ""}${minutes}`;
  }
  return notificationDate.toDateString();
};

const Notification = props => {
  const { notification } = props;

  return (
    <View style={styles.notification}>
      <View style={styles.inlining}>
        <View
          style={[styles.circle, notification.read ? styles.circleread : {}]}
        />
        <Text
          style={[styles.from, !notification.read ? styles.unreadText : {}]}
        >
          {notification.from.name}
        </Text>
        <View style={{ flex: 1, flexDirection: "column" }}>
          <Text style={{ alignSelf: "flex-end" }}>
            {showDate(notification)}
          </Text>
        </View>
      </View>
      <Text
        style={[styles.subject, !notification.read ? styles.unreadText : {}]}
      >
        {notification.subject}
      </Text>
      <Text numberOfLines={2} style={styles.text}>
        {notification.body}
      </Text>
    </View>
  );
};

Notification.propTypes = {
  notification: PropTypes.shape({
    read: PropTypes.bool.isRequired,
    from: PropTypes.shape({
      name: PropTypes.string.isRequired
    }).isRequired,
    subject: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired
  }).isRequired
};

export default Notification;
