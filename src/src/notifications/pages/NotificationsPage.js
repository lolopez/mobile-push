import React, { Component } from "react";
import {
  StyleSheet,
  View,
  DeviceEventEmitter,
  PushNotificationIOS
} from "react-native";
import PropTypes from "prop-types";
import NotificationsListContainer from "../containers/components/NotificationsListContainer";

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default class NotificationsPage extends Component {
  constructor(props) {
    super(props);
    DeviceEventEmitter.addListener("onMessage", () => {
      this.onMessageReceive();
    });

    PushNotificationIOS.addEventListener("notification", () => {
      this.onMessageReceive();
    });
  }

  componentWillMount() {
    const { findAllNotifications } = this.props;
    findAllNotifications();
  }

  onMessageReceive() {
    const { clearCache, findAllNotifications } = this.props;
    clearCache();
    findAllNotifications();
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <NotificationsListContainer navigation={navigation} />
      </View>
    );
  }
}

NotificationsPage.propTypes = {
  clearCache: PropTypes.func.isRequired,
  findAllNotifications: PropTypes.func.isRequired
};
