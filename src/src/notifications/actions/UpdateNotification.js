import { RSAA } from "redux-api-middleware";
import { BASE_URL } from "react-native-dotenv";
import { withAuth } from "../../auth/utils/authUtils";

// Get notifications
export const UPDATE_NOTIFICATION = "UPDATE_NOTIFICATION";
export const UPDATE_NOTIFICATION_SUCCESS = "UPDATE_NOTIFICATION_SUCCESS";
export const UPDATE_NOTIFICATION_FAILURE = "UPDATE_NOTIFICATION_FAILURE";

export const updateNotification = userNotification => {
  return {
    [RSAA]: {
      endpoint: `${BASE_URL}/user/notifications/${userNotification.id}`,
      method: "PUT",
      body: JSON.stringify({ userNotification }),
      credentials: "include",
      headers: withAuth({ "Content-Type": "application/json" }),
      types: [
        UPDATE_NOTIFICATION,
        UPDATE_NOTIFICATION_SUCCESS,
        UPDATE_NOTIFICATION_FAILURE
      ]
    }
  };
};
