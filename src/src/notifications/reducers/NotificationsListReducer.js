import {
  FETCH_START,
  FETCH_SUCCESS,
  FETCH_ERROR
} from "redux-cached-api-middleware/es/actionTypes";
import { NOTIFICATIONS_KEY } from "../actions/FindAllNotifications";
import { UPDATE_NOTIFICATION_SUCCESS } from "../actions/UpdateNotification";
import { UPDATE_FILTER_NOTIFCATIONS_FUNCTION } from "../actions/UpdateFilterNotificationsFunction";

const INITIAL_STATE = {
  notifications: [],
  archivedNotifications: [],
  filterFunction: () => true,
  error: null,
  loading: false
};

function processGetNotifications(state) {
  return {
    ...state,
    loading: true
  };
}

function processGetNotificationsSuccess(state, notifications) {
  return {
    ...state,
    notifications: notifications.filter(n => !n.archived),
    archivedNotifications: notifications.filter(n => n.archived),
    error: null,
    loading: false
  };
}

function processGetNotificationsFailure(state, error) {
  return {
    ...state,
    notifications: [],
    archivedNotifications: [],
    error,
    loading: false
  };
}

function processUpdateNotificationSuccess(state, notification) {
  const notifications = state.notifications.map(r =>
    r.id === notification.id
      ? { ...r, read: notification.read, archived: notification.archived }
      : r
  )

  return {
    ...state,
    notifications: notifications.filter(n => !n.archived),
    archivedNotifications: notifications.filter(n => n.archived),     
  };

}

function processUpdateFilterNotificationsFunction(state, filterFunction) {
  return {
    ...state,
    filterFunction: filterFunction || (() => true)
  };
}

export default function(state = INITIAL_STATE, action) {
  let error;

  if (action.meta) {
    if (action.meta.cache.key === NOTIFICATIONS_KEY) {
      switch (action.type) {
        case FETCH_START:
          return processGetNotifications(state);
        case FETCH_SUCCESS:
          return processGetNotificationsSuccess(state, action.payload);
        case FETCH_ERROR:
          error = action.payload || { message: action.payload.message };
          return processGetNotificationsFailure(state, error);

        default:
          return state;
      }
    }
  } else {
    switch (action.type) {
      case UPDATE_NOTIFICATION_SUCCESS:
        return processUpdateNotificationSuccess(state, action.payload);

      case UPDATE_FILTER_NOTIFCATIONS_FUNCTION:
        return processUpdateFilterNotificationsFunction(state, action.payload);

      default:
        return state;
    }
  }
  return state;
}
