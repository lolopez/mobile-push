import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableHighlight,
  ScrollView
} from "react-native";
import HTMLView from "react-native-htmlview";

const styles = StyleSheet.create({
  fromText: {
    marginTop: 2,
    fontWeight: "bold",
    fontSize: 22,
    color: "black"
  },
  subject: {
    marginTop: 8,
    marginLeft: 4,
    fontWeight: "bold",
    fontSize: 18,
    color: "black"
  },
  text: {
    marginTop: 4,
    marginLeft: 4
  },
  view: {
    backgroundColor: "white",
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 8,
    marginBottom: 8
  },
  circle: {
    marginTop: 5,
    marginRight: 8,
    width: 40,
    height: 40,
    backgroundColor: "#2196F3",
    borderRadius: 45,
    justifyContent: "center",
    alignItems: "center"
  },
  inline: {
    flexDirection: "row",
    alignSelf: "flex-start"
  }
});

export default class NotificationDetails extends Component {
  static showDate(notification) {
    const notificationDate = new Date(notification.sentAt);
    const today = new Date();
    const hours = notificationDate.getHours();
    const minutes = notificationDate.getMinutes();

    if (notificationDate.toDateString() === today.toDateString()) {
      return `${hours}:${minutes < 10 ? "0" : ""}${minutes}`;
    }
    return notificationDate.toDateString();
  }

  constructor(props) {
    super(props);
    this.onClickUnread = this.unread.bind(this, "onClickUnread");
    this.onClickArchive = this.archive.bind(this, "onClickArchive");
    this.onClickCreateRule = this.createRule.bind(this, "onClickCreateRule");

    const { navigation } = this.props;

    navigation.setParams({
      onClickUnread: this.onClickUnread,
      onClickArchive: this.onClickArchive,
      onClickCreateRule: this.onClickCreateRule

    });
  }

  updateNotification(variable, value) {
    const { navigation } = this.props;
    const { state } = navigation;
    const params = state.params || {};
    const notification = navigation.getParam("notification");
    notification[variable] = value;
    params.update(notification);
    navigation.goBack();
  }

  unread() {
    this.updateNotification("read", false);
  }

  createRule() {
    const { navigation } = this.props;
    const notification = navigation.getParam("notification");
    navigation.navigate('CreateRulePage', { notification: notification });
  }

  archive() {
    this.updateNotification("archived", true);
  }

  render() {
    const { navigation } = this.props;
    const notification = navigation.getParam("notification");
    const notifications = navigation.getParam("notifications");

    return (
      <ScrollView style={{ backgroundColor: "white" }}>
        <View style={styles.view}>
          <View style={styles.inline}>
            <TouchableHighlight
              onPress={() =>
                navigation.navigate("NotificationsList", {
                  notifications: notifications.filter(
                    n => n.from.name === notification.from.name
                  )
                })
              }
            >
              <View style={styles.circle}>
                <Text style={{ color: "white" }}>
                  {notification.from.name.charAt(0).toUpperCase()}
                </Text>
              </View>
            </TouchableHighlight>
            <View>
              <Text style={styles.fromText}>{notification.from.name}</Text>
              <Text>{NotificationDetails.showDate(notification)}</Text>
            </View>
          </View>
          <Text style={styles.subject}>{notification.subject}</Text>
          <HTMLView style={styles.text} value={notification.body} />
        </View>
      </ScrollView>
    );
  }
}
