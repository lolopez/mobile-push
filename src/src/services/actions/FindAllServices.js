import { RSAA } from "redux-api-middleware";
import { BASE_URL } from "react-native-dotenv";
import { withAuth } from "../../auth/utils/authUtils";

// Get notifications
export const GET_SERVICES = "GET_SERVICES";
export const GET_SERVICES_SUCCESS = "GET_SERVICES_SUCCESS";
export const GET_SERVICES_FAILURE = "GET_SERVICES_FAILURE";

export const findAllServices = () => ({
  [RSAA]: {
    endpoint: `${BASE_URL}/services`,
    method: "GET",
    credentials: "include",
    headers: withAuth({ "Content-Type": "application/json" }),
    types: [GET_SERVICES, GET_SERVICES_SUCCESS, GET_SERVICES_FAILURE]
  }
});
