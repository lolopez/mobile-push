import React from "react";
import { createStackNavigator } from "react-navigation";
import Icon from "react-native-vector-icons/Ionicons";
import HeaderButtons, {
  HeaderButton,
  Item
} from "react-navigation-header-buttons";
import NotificationsPageContainer from "../../notifications/containers/pages/NotificationsPageContainer";
import NotificationDetails from "../../notifications/components/NotificationDetails";
import FilterNotificationsMenuContainer from "../../notifications/containers/components/FilterNotificationsMenuContainer";

const IoniconsHeaderButton = passMeFurther => (
  // the `passMeFurther` variable here contains props from <Item .../> as well as <HeaderButtons ... />
  // and it is important to pass those props to `HeaderButton`
  // then you may add some information like icon size or color (if you use icons)
  <HeaderButton
    {...passMeFurther}
    IconComponent={Icon}
    iconSize={25}
    color="white"
  />
);

export default createStackNavigator(
  {
    NotificationsPageContainer: {
      screen: NotificationsPageContainer,
      navigationOptions: () => {
        return {
          headerTitleStyle: {
            color: "white"
          },
          title: `CERN Notification Center`,
          headerStyle: {
            backgroundColor: "#2196F3"
          },
          headerRight: <FilterNotificationsMenuContainer />
        };
      }
    },
    NotificationDetails: {
      screen: NotificationDetails,
      navigationOptions: ({ navigation }) => {
        const { params = {} } = navigation.state;
        return {
          headerTitleStyle: {
            color: "white"
          },
          title: `Details`,
          headerStyle: {
            backgroundColor: "#2196F3"
          },
          headerTintColor: "white",
          headerRight: (
            <HeaderButtons HeaderButtonComponent={IoniconsHeaderButton}>
              <Item
                title="delete"
                iconName="md-mail-unread"
                onPress={() => params.onClickUnread()}
              />
              <Item
                title="save"
                iconName="md-archive"
                onPress={() => params.onClickArchive()}
              />
            </HeaderButtons>
          )
        };
      }
    }
  },
  {}
);
