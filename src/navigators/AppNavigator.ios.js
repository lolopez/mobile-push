import React from "react";
import { createBottomTabNavigator } from "react-navigation";
import Ionicons from "react-native-vector-icons/Ionicons";
import NotificationsStack from "./stacks/NotificationsStack";
import SettingsStack from "./stacks/SettingsStack";
import SearchStack from "./stacks/SearchStack";

function getIosIcon(iconName, focused, tintColor = "#fff") {
  return (
    <Ionicons name={`ios-${iconName}`} size={25} color={tintColor} solid />
  );
}

const NotificationsListScreenOptions = () => {
  return {
    tabBarIcon: ({ focused, tintColor }) => {
      const iconName = "notifications";
      return getIosIcon(iconName, focused, tintColor);
    },
    tabBarLabel: "Notifications",
    title: "Notifications"
  };
};

const CreateRulePageScreenOptions = () => {
  return {
    tabBarIcon: ({ focused, tintColor }) => {
      const iconName = "settings";

      return getIosIcon(iconName, focused, tintColor);
    },
    tabBarLabel: "Settings",
    title: "Settings"
  };
};

const SearchPageOptions = () => {
  return {
    tabBarIcon: ({ focused, tintColor }) => {
      const iconName = "search";

      return getIosIcon(iconName, focused, tintColor);
    },
    tabBarLabel: "Search",
    title: "Search"
  };
};

const IosApp = createBottomTabNavigator(
  {
    NotificationsListPage: {
      screen: NotificationsStack,
      navigationOptions: NotificationsListScreenOptions
    },
    Settings: {
      screen: SettingsStack,
      navigationOptions: CreateRulePageScreenOptions
    },
    Search: { screen: SearchStack, navigationOptions: SearchPageOptions }
  },
  {
    tabBarOptions: {
      activeTintColor: "#fff",
      inactiveTintColor: "#555",
      style: {
        backgroundColor: "#2185d0"
      }
    }
  }
);

export default IosApp;
