import { combineReducers } from "redux";
import newRule from "./NewRule";
import rulesList from "./RulesListReducers";

const rulesReducers = combineReducers({
  newRule,
  rulesList
});

export default rulesReducers;
