import React, { Component } from "react";
import { Text, View, StyleSheet, Alert } from "react-native";
import { TextField } from "react-native-material-textfield";
import { RadioButton } from "react-native-paper";
import PropTypes from "prop-types";

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1,
    padding: 10
  },
  radioBtn: {
    flexDirection: "row",
    alignItems: "center"
  }
});

class EditRulePage extends Component {
  state = {
    id: undefined,
    name: "",
    string: "",
    channel: "",
    type: ""
  };

  constructor(props) {
    super(props);

    const { navigation } = this.props;

    this.onClickSave = this.save.bind(this, "onClickSave");
    this.onClickDelete = this.delete.bind(this, "onClickDelete");

    const rule = navigation.getParam("rule");

    this.state.id = rule.id;
    this.state.name = rule.name;
    this.state.string = rule.string;
    this.state.channel = rule.channel;
    this.state.type = rule.type;

    navigation.setParams({
      onClickSave: this.onClickSave,
      onClickDelete: this.onClickDelete
    });
  }

  save() {
    const { updateRule, navigation } = this.props;

    updateRule({ ...this.state });
    navigation.goBack();
  }

  delete() {
    const { deleteRule, navigation } = this.props;
    const { id } = this.state;

    Alert.alert("Delete rule", "Are you sure you want to delete this rule?", [
      {
        text: "Cancel",
        style: "cancel"
      },
      {
        text: "Accept",
        onPress: () => {
          deleteRule(id);
          navigation.goBack();
        }
      }
    ]);
  }

  render() {
    const { name, string, channel, type } = this.state;
    return (
      <View style={styles.container}>
        <TextField
          label="Rule name"
          value={name}
          onChangeText={newName => this.setState({ name: newName })}
        />
        {type === "FromContains" && (
          <TextField
            label="String"
            value={string}
            onChangeText={newString => this.setState({ string: newString })}
          />
        )}
        <Text>Channel</Text>
        <RadioButton.Group
          onValueChange={newChannel => this.setState({ channel: newChannel })}
          value={channel}
        >
          <View style={styles.radioBtn}>
            <RadioButton value="PUSH" />
            <Text>Push</Text>
          </View>
          <View style={styles.radioBtn}>
            <RadioButton value="EMAIL" />
            <Text>Email</Text>
          </View>
        </RadioButton.Group>
      </View>
    );
  }
}

EditRulePage.propTypes = {
  deleteRule: PropTypes.func.isRequired,
  updateRule: PropTypes.func.isRequired
};

export default EditRulePage;
