import React from "react";
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  Dimensions
} from "react-native";
import PropTypes from "prop-types";

import Separator from "../../common/Separator";

const widtha = Dimensions.get("window").width; // full width

const styles = StyleSheet.create({
  text: {
    color: "black",
    fontSize: 20,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 8,
    marginBottom: 8,
    width: widtha * 0.95
  }
});

const ServicesList = props => {
  const { services, navigation, notifications, updateNotification } = props;
  return (
    <FlatList
      data={services.map((n, i) => ({ key: i.toString(), service: n }))}
      renderItem={({ item }) => (
        <TouchableWithoutFeedback
          onPress={() =>
            navigation.navigate("NotificationsList", {
              updateNotification,
              notifications: notifications.filter(
                n => n.from.name === item.service.name
              )
            })
          }
        >
          <Text style={styles.text} key={item.key}>
            {item.service.name}
          </Text>
        </TouchableWithoutFeedback>
      )}
      ItemSeparatorComponent={Separator}
    />
  );
};

ServicesList.propTypes = {
  services: PropTypes.arrayOf(Object).isRequired,
  notifications: PropTypes.arrayOf(Object).isRequired,
  updateNotification: PropTypes.func.isRequired
};

export default ServicesList;
